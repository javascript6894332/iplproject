// Import necessary modules and libraries
const deliveriesPath = "../data/deliveries.csv"; // Path to the CSV file containing delivery data
const matchesPath = "../data/matches.csv";
const outputPath = "../public/output/4-10Economical-bowlers-2015.json"; // Path to the output JSON file
const fs = require("fs"); // Import the Node.js file system module
const deliveriesCsv = require("csvtojson"); // Import the csvtojson library for converting CSV to JSON
const matchesCsv = require("csvtojson");

function topTenEconomicalBowler15() {
  deliveriesCsv()
    .fromFile(deliveriesPath)
    .then((deliveriesJson) => {
      const deliveriesData = deliveriesJson;
      matchesCsv()
        .fromFile(matchesPath)
        .then((matchesJson) => {
          const matchData = matchesJson;
          const id_2015 = {};
          const economy_list = {};
          const bowlersOvers = {};
          for (
            let match_index = 0;
            match_index < matchData.length;
            match_index++
          ) {
            if (matchData[match_index].season == 2015) {
              id_2015[matchData[match_index].id] = 1;
            }
          }
          for (
            let deliveryIndex = 0;
            deliveryIndex < deliveriesData.length;
            deliveryIndex++
          ) {
            const bowler = deliveriesData[deliveryIndex].bowler;
            const runs = deliveriesData[deliveryIndex].total_runs;
            if (deliveriesData[deliveryIndex].match_id in id_2015) {
              if (economy_list[bowler]) {
                economy_list[bowler] += parseFloat(runs);
              } else {
                economy_list[bowler] = parseFloat(runs);
              }
              if (bowlersOvers[bowler]) {
                bowlersOvers[bowler] += 1;
              } else {
                bowlersOvers[bowler] = 1;
              }
            }
          }
          const bowlersEconomy = {};

          for (bowlerName in bowlersOvers) {
            bowlersEconomy[bowlerName] =
              economy_list[bowlerName] / (bowlersOvers[bowlerName] / 6);
          }
          const economyAscending = Object.entries(bowlersEconomy).sort(
            (a, b) => {
              return a[1] - b[1];
            }
          );
          const result = [];
          for (let outerIndex = 0; outerIndex < 10; outerIndex++) {
            const name = economyAscending[outerIndex][0];
            const economy_rate = economyAscending[outerIndex][1];
            result.push({ name, economy_rate });
          }

          fs.writeFile(outputPath, JSON.stringify(result, null, 2), (error) => {
            if (error) {
              console.error(`The file produced an error: ${error}`);
            } else {
              console.log(`Successfully exported output to ${outputPath}`);
            }
          });
        })
        .catch((err) => {
          console.error(err); // Handle any errors that occur during the CSV to JSON conversion or file writing
        });
    })
    .catch((err) => {
      console.error(err); // Handle any errors that occur during the CSV to JSON conversion or file writing
    });
}
topTenEconomicalBowler15();
