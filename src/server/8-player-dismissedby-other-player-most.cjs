// Import necessary modules and libraries
const deliveriesPath = "../data/deliveries.csv"; // Path to the CSV file containing delivery data

const outputPath =
  "../public/output/8-player-dismissedby-other-player-most.json"; // Path to the output JSON file
const fs = require("fs"); // Import the Node.js file system module
const deliveriesCsv = require("csvtojson"); // Import the csvtojson library for converting CSV to JSON

function playerDismissedMostbyBowler() {
  deliveriesCsv()
    .fromFile(deliveriesPath)
    .then((deliveriesJson) => {
      const deliveryData = deliveriesJson;
      const batsmanList = {};
      let result = ["x", ["y", 0]];
      for (
        let deliveryIndex = 0;
        deliveryIndex < deliveryData.length;
        deliveryIndex++
      ) {
        const batsman = deliveryData[deliveryIndex].player_dismissed;
        const bowler = deliveryData[deliveryIndex].bowler;
        const isDismissed =
          deliveryData[deliveryIndex].dismissal_kind === "run out" ? 0 : 1;
        if (batsmanList[batsman]) {
          if (batsmanList[batsman][bowler] && isDismissed) {
            batsmanList[batsman][bowler] += 1;
            if (result[1][1] <= batsmanList[batsman][bowler] && batsman) {
              result = [batsman, [bowler, batsmanList[batsman][bowler]]];
            }
          } else {
            if (isDismissed) {
              batsmanList[batsman][bowler] = 1;
            }
          }
        } else {
          batsmanList[batsman] = {};
          if (isDismissed) {
            batsmanList[batsman][bowler] = 1;
          }
        }
      }
      let name = result[1][0];
      let times = result[1][1];
      result[1] = { name, times };

      fs.writeFile(outputPath, JSON.stringify(result, null, 2), (error) => {
        if (error) {
          console.error(`The file produced an error: ${error}`);
        } else {
          console.log(`Successfully exported output to ${outputPath}`);
        }
      });
    })
    .catch((err) => {
      console.error(err); // Handle any errors that occur during the CSV to JSON conversion or file writing
    });
}
playerDismissedMostbyBowler();
