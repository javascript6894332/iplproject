// Import necessary modules and libraries
const matchesPath = "../data/matches.csv"; // Path to the CSV file containing match data
const outputPath = "../public/output/5-team-toss+match-won.json"; // Path to the output JSON file
const fs = require("fs"); // Import the Node.js file system module
const matchesCsv = require("csvtojson"); // Import the csvtojson library for converting CSV to JSON

function teamWinningTossAndMatch() {
  // Read the CSV file and convert it to JSON using csvtojson
  matchesCsv()
    .fromFile(matchesPath)
    .then((matchesJson) => {
      const matchesData = matchesJson; // Store the JSON data representing match information
      const result = {};
      for (let matchIndex = 0; matchIndex < matchesData.length; matchIndex++) {
        const tossWin = matchesData[matchIndex].toss_winner;
        const matchWin = matchesData[matchIndex].winner;
        if (result[tossWin]) {
          if (tossWin === matchWin) {
            result[tossWin] += 1;
          }
        } else {
          if (tossWin === matchWin) {
            result[tossWin] = 1;
          }
        }
      }

      fs.writeFile(outputPath, JSON.stringify(result, null, 2), (error) => {
        if (error) {
          console.error(`The file produced an error: ${error}`);
        } else {
          console.log(`Successfully exported output to ${outputPath}`);
        }
      });
    })
    .catch((err) => {
      console.error(err); // Handle any errors that occur during the CSV to JSON conversion or file writing
    });
}
teamWinningTossAndMatch();
