// Import necessary modules and libraries
const deliveriesPath = "../data/deliveries.csv"; // Path to the CSV file containing delivery data

const outputPath =
  "../public/output/9-super-over-best-economy-bowler.json"; // Path to the output JSON file
const fs = require("fs"); // Import the Node.js file system module
const deliveriesCsv = require("csvtojson"); // Import the csvtojson library for converting CSV to JSON

function bestSuperOverBowler() {
  deliveriesCsv()
    .fromFile(deliveriesPath)
    .then((deliveriesJson) => {
      const deliveriesData = deliveriesJson;
      const superOverBowlers = {};
      const result = [["x", 100000]];
      for (
        let deliveryIndex = 0;
        deliveryIndex < deliveriesData.length;
        deliveryIndex++
      ) {
        const isSuper = Number(deliveriesData[deliveryIndex].is_super_over);
        const bowler = deliveriesData[deliveryIndex].bowler;
        const runs = Number(deliveriesData[deliveryIndex].total_runs);
        if (isSuper) {
          if (bowler in superOverBowlers) {
            superOverBowlers[bowler][0] += runs;
            superOverBowlers[bowler][1] += 1;
          } else {
            superOverBowlers[bowler] = [runs, 1];
          }
        }
      }
      for (bowlerName in superOverBowlers) {
        superOverBowlers[bowlerName] =
          (superOverBowlers[bowlerName][0] * 6) /
          superOverBowlers[bowlerName][1];
        if (superOverBowlers[bowlerName] < result[0][1]) {
          result[0][0] = bowlerName;
          result[0][1] = superOverBowlers[bowlerName];
        }
      }
      const bowler = result[0][0];
      const economy = result[0][1];
      result[0] = { bowler, economy };
      fs.writeFile(outputPath, JSON.stringify(result, null, 2), (error) => {
        if (error) {
          console.error(`The file produced an error: ${error}`);
        } else {
          console.log(`Successfully exported output to ${outputPath}`);
        }
      });
    })
    .catch((err) => {
      console.error(err); // Handle any errors that occur during the CSV to JSON conversion or file writing
    });
}
bestSuperOverBowler();
