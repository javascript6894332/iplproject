// Import necessary modules and libraries
const deliveriesPath = "../data/deliveries.csv"; // Path to the CSV file containing delivery data
const matchesPath = "../data/matches.csv";
const outputPath = "../public/output/7-strike-rate-of-batsman-per-year.json"; // Path to the output JSON file
const fs = require("fs"); // Import the Node.js file system module
const deliveriesCsv = require("csvtojson"); // Import the csvtojson library for converting CSV to JSON
const matchesCsv = require("csvtojson");

function strikeRateOfBatsmanPerYear() {
  deliveriesCsv()
    .fromFile(deliveriesPath)
    .then((deliveriesJson) => {
      const deliveriesData = deliveriesJson;
      matchesCsv()
        .fromFile(matchesPath)
        .then((matchesJson) => {
          const matchData = matchesJson;
          const result = {};
          const matchYearId = {};
          for (
            let matchIndex = 0;
            matchIndex < matchData.length;
            matchIndex++
          ) {
            const season = matchData[matchIndex].season;
            const matchId = matchData[matchIndex].id;
            matchYearId[matchId] = season;
          }
          for (
            let deliveryIndex = 0;
            deliveryIndex < deliveriesData.length;
            deliveryIndex++
          ) {
            const batsman = deliveriesData[deliveryIndex].batsman;
            const deliveryId = deliveriesData[deliveryIndex].match_id;
            const year = matchYearId[deliveryId];
            if (batsman in result) {
              const runs =
                Number(deliveriesData[deliveryIndex].total_runs) -
                Number(deliveriesData[deliveryIndex].extra_runs);
              if (year in result[batsman]) {
                result[batsman][year][0] += runs;
                result[batsman][year][1] += 1;
              } else {
                result[batsman][year] = [runs, 1];
              }
            } else {
              const runs =
                Number(deliveriesData[deliveryIndex].total_runs) -
                Number(deliveriesData[deliveryIndex].extra_runs);
              result[batsman] = {};
              result[batsman][year] = [runs, 1];
            }
          }
          for (playerName in result) {
            for (iplYear in result[playerName]) {
              result[playerName][iplYear] = Math.round(
                (result[playerName][iplYear][0] /
                  result[playerName][iplYear][1]) *
                  100
              );
            }
          }
          fs.writeFile(outputPath, JSON.stringify(result, null, 2), (error) => {
            if (error) {
              console.error(`The file produced an error: ${error}`);
            } else {
              console.log(`Successfully exported output to ${outputPath}`);
            }
          });
        })
        .catch((err) => {
          console.error(err); // Handle any errors that occur during the CSV to JSON conversion or file writing
        });
    })
    .catch((err) => {
      console.error(err); // Handle any errors that occur during the CSV to JSON conversion or file writing
    });
}
strikeRateOfBatsmanPerYear();
